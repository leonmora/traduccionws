<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get posted data
$data = json_decode(file_get_contents("php://input"));
$met = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];
$array = array("ichi" => 1, "ni" => 2, "san" => 3, "shi" => 4, "go"  => 5, "roku" => 6, "sichi" => 7, "hachi" => 8, "ku" => 9, "ju" => 10);


if(
    !empty($data->num)
){
    if($met == 'POST'){
        http_response_code(201);
        $traduccionJaponesa = array_search($data->num, $array);

        echo "traducido como";

        echo json_encode(array(
        "translation:" => $traduccionJaponesa
        ));

    }
}
?>